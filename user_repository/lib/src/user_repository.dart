import 'dart:async';

import 'package:meta/meta.dart';
import 'package:flutter_login/api/api.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class UserRepository {
  Future<String> authenticate({
    @required String username,
    @required String password,
  }) async {
    final token = await Future.delayed(Duration(seconds: 1));
    return token;
//    Map body = {"usernameOrEmail": username, "password": password};
//    final rs = await ApiService.userLogin(body);
//    if (rs.toString() == "400") {
//      return rs;
//    } else {
//      final token =
//          json.decode(rs)["tokenType"] + ' ' + json.decode(rs)["accessToken"];
//      // final user = json.decode(rs)["user"]["username"];
//      SharedPreferences perferences = await SharedPreferences.getInstance();
//      perferences.setString("token", token);
//      return token.toString();
//    }
    //perferences.setString("username", user);
  }

  Future<void> deleteToken() async {
    /// delete from keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    SharedPreferences perferences = await SharedPreferences.getInstance();
    perferences.remove("token");
    perferences.remove("username");
    return;
  }

  Future<void> persistToken(String token) async {
    /// write to keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return;
  }

  Future<bool> hasToken() async {
    /// read from keystore/keychain
    await Future.delayed(Duration(seconds: 1));
    return false;
  }
}
