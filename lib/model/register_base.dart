class RegisterBase {
  final String name;
  final String email;
  final String username;
  final String password;

  const RegisterBase({this.name, this.email, this.username, this.password});

//  bool isInQuery(String query) {
//    return (username.toLowerCase().contains(query.toLowerCase()) ||
//        password.toLowerCase().contains(query.toLowerCase()));
//  }

  RegisterBase copyWith({name, email, username}) => RegisterBase(
      name: name ?? this.name,
      email: email ?? this.email,
      username: username ?? this.username,
      password: password ?? this.password);
}

class UserInfoBase {
  final String fullname;
  final String username;
  final String password;

  const UserInfoBase({this.fullname, this.username, this.password});
  UserInfoBase copyWith({fullname, username, password}) => UserInfoBase(
      fullname: fullname ?? this.fullname,
      username: username ?? this.username,
      password: password ?? this.password);
}

class UserContactBase {
  final String mobileNumber;
  final String email;
  final String facebook;

  const UserContactBase({this.mobileNumber, this.email, this.facebook});
  UserContactBase copyWith({mobileNumber, email, facebook}) => UserContactBase(
      mobileNumber: mobileNumber ?? this.mobileNumber,
      email: email ?? this.email,
      facebook: facebook ?? this.facebook);
}
