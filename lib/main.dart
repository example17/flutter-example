import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_repository/user_repository.dart';

import 'package:flutter_login/authentication/authentication.dart';
import 'package:flutter_login/splash/splash.dart';
import 'package:flutter_login/features/login/login.dart';
import 'package:flutter_login/features/home/home.dart';
import 'package:flutter_login/common/common.dart';
import 'styles/main_styles.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final userRepository = UserRepository();
  runApp(EasyLocalization(
    child: BlocProvider<AuthenticationBloc>(
      create: (context) {
        return AuthenticationBloc(userRepository: userRepository)
          ..add(AppStarted());
      },
      child: App(userRepository: userRepository),
    ),
  ));
}

class App extends StatelessWidget {
  final UserRepository userRepository;

  App({Key key, @required this.userRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;

    return EasyLocalizationProvider(
        data: data,
        child: MaterialApp(
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            EasyLocalizationDelegate(
                locale: data.locale, path: 'lib/resources/langs')
          ],
          supportedLocales: [Locale('lo', 'LA'), Locale('en', 'US')],
          locale: data.savedLocale,
          theme: MainStyles.themeData,
          // SearchScreen()
          home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
            // ignore: missing_return
            builder: (context, state) {
              if (state is AuthenticationUninitialized) {
                return SplashPage();
              }
              if (state is AuthenticationAuthenticated) {
                return HomePage();
              }
              if (state is AuthenticationUnauthenticated) {
                return LoginPage(userRepository: userRepository);
              }
              if (state is AuthenticationLoading) {
                return LoadingIndicator();
              }
            },
          ),
        ));

//    return MaterialApp(
//      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
//        // ignore: missing_return
//        builder: (context, state) {
//          if (state is AuthenticationUninitialized) {
//            return SplashPage();
//          }
//          if (state is AuthenticationAuthenticated) {
//            return HomePage();
//          }
//          if (state is AuthenticationUnauthenticated) {
//            return LoginPage(userRepository: userRepository);
//          }
//          if (state is AuthenticationLoading) {
//            return LoadingIndicator();
//          }
//        },
//      ),
//    );
  }
}
