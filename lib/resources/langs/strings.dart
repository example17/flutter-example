class S {
  static const String TITLE = "labels.title";
  static const String LYRICS = "labels.lyrics";
  static const String ARTIST = "labels.artist";
  static const String ADD_SONG = "labels.add_song";
  static const String SONG_DETAILS = "labels.song_details";
  static const String SONG_LYRICS = "labels.song_lyrics";
  static const String SEARCH_LYRICS = "labels.search_lyrics";
  static const String ENTER_SONG_TITLE = "labels.enter_song_title";
  static const String EDIT = "labels.edit";
  static const String EDIT_SONG = "labels.edit_song";

  static const String EMPTY_TITLE = "errors.empty_title";
  static const String EMPTY_LYRICS = "errors.empty_lyrics";
  static const String EMPTY_ARTIST = "errors.empty_artist";
  static const String EMPTY_LIST = "errors.empty_list";

  // error login
  static const String EMPTY_PASSWORD = "errors.empty_password";
  static const String EMPTY_USERNAME = "errors.empty_username";

  //login
  static const String USERNAME = "labels.username";
  static const String PASSWORD = "labels.password";
  static const String LOGIN = "labels.login";
  static const String REGISTER = "labels.register";

  static const String RESET_PASSWORD = "labels.reset_password";
}
