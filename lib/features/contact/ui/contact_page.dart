import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_login/features/contact/bloc/bloc.dart';
import 'package:flutter_login/features/contact/ui/contact_form.dart';

class ContactPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contact'),
      ),
      body: BlocProvider(
          create: (context) {
            return ContactBloc();
          },
          child: Stack(
            children: <Widget>[
              Center(
                child: ContactForm(),
              ),
            ],
          )),
    );
  }
}
