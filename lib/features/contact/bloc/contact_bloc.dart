import 'dart:async';

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_login/features/contact/bloc/bloc.dart';
import 'package:flutter_login/model/register_base.dart';
import 'package:flutter_login/api/api.dart';

class ContactBloc extends Bloc<ContactEvent, ContactState> {
  ContactBloc();
  @override
  // TODO: implement initialState
  ContactState get initialState => ContactInitial();

  @override
  Stream<ContactState> mapEventToState(ContactEvent event) async* {
    if (event is GetUserInfo) {
      yield ContactInitial();
      yield* _mapRegisterToState(event);
    }
  }

  Stream<ContactState> mapDispatchToState(ContactEvent event) async* {}

  Stream<ContactState> _mapRegisterToState(GetUserInfo event) async* {
    yield ContactProcessing();

    try {
      final String userInfo = await ApiService.userInfo();
      yield ContactSuccess(info: userInfo.toString());
    } catch (error) {
      yield ContactFailure(error: error.toString());
    }
  }
}
