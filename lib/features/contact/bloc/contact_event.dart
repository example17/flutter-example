import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class ContactEvent extends Equatable {
  const ContactEvent();
  @override
  List<Object> get props => [];
}

class GetUserInfo extends ContactEvent {}
