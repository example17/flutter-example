import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class ContactState extends Equatable {
  const ContactState();

  @override
  List<Object> get props => [];
}

class ContactInitial extends ContactState {
  @override
  String toString() => 'StateEmpty';
}

class ContactProcessing extends ContactState {
  @override
  String toString() => 'StateProcessing';
}

class ContactSuccess extends ContactState {
  final String info;

  const ContactSuccess({@required this.info});

  @override
  List<Object> get props => [info];

  @override
  String toString() => 'Success { success: $info }';
}

class ContactFailure extends ContactState {
  final String error;

  const ContactFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'Failure { error: $error }';
}
