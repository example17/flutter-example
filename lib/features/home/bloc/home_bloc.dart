import 'dart:async';

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_login/features/home/bloc/bloc.dart';
import 'package:flutter_login/model/register_base.dart';
import 'package:flutter_login/api/api.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc();
  @override
  // TODO: implement initialState
  HomeState get initialState => HomeInitial();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is GetUserInfo) {
      yield HomeInitial();
      yield* _mapRegisterToState(event);
    }
  }

  Stream<HomeState> mapDispatchToState(HomeEvent event) async* {}

  Stream<HomeState> _mapRegisterToState(GetUserInfo event) async* {
    yield HomeProcessing();

    try {
      //final String userInfo = await ApiService.userInfo();
      final String userInfo = "{}";
      yield HomeSuccess(info: userInfo.toString());
    } catch (error) {
      yield HomeFailure(error: error.toString());
    }
  }
}
