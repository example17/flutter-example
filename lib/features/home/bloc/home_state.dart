import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {
  @override
  String toString() => 'StateEmpty';
}

class HomeProcessing extends HomeState {
  @override
  String toString() => 'StateProcessing';
}

class HomeSuccess extends HomeState {
  final String info;

  const HomeSuccess({@required this.info});

  @override
  List<Object> get props => [info];

  @override
  String toString() => 'Success { success: $info }';
}

class HomeFailure extends HomeState {
  final String error;

  const HomeFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'Failure { error: $error }';
}
