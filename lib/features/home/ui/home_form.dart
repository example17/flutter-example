import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_login/features/home/bloc/bloc.dart';
import 'package:flutter_login/widgets/buttons.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter_login/authentication/authentication.dart';

class HomeForm extends StatefulWidget {
  @override
  State<HomeForm> createState() => _HomeFormState();
}

class _HomeFormState extends State<HomeForm> {
  @override
  _showDialog(BuildContext context, title, description, AlertType alertType) {
    // flutter defined function
    // BuildContext context;
    Alert(
      context: context,
      type: alertType,
      title: title,
      desc: description,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }

  String id = '1';
  String fullName = 'jack sparrow';
  String phone = '02000000000';
  String username = 'jack';
  String rs = '';

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<HomeBloc>(context).add(GetUserInfo());
    _onLoginButtonPressed() {
      BlocProvider.of<HomeBloc>(context).add(GetUserInfo());
    }

    return BlocListener<HomeBloc, HomeState>(
      listener: (context, state) {
//        if (state is HomeFailure) {
//          Scaffold.of(context).showSnackBar(
//            SnackBar(
//              content: Text('${state.error}'),
//              backgroundColor: Colors.red,
//            ),
//          );
//        } else if (state is HomeSuccess) {
//          rs = state.info;
//          id = json.decode(rs)["id"].toString();
//          fullName = json.decode(rs)["name"].toString();
//          username = json.decode(rs)["username"].toString();
//          phone = "02099132498";
////          this._showDialog(
////              context,
////              'Success',
////              'Successfully! \nresponse Code: ' + state.info,
////              AlertType.success);
//        }
      },
      child: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          return Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'WELCOME',
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  'Id: ' + id,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  'FullName: ' + fullName,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  'PhoneNumber: ' + phone,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  'Username: ' + username,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                    padding: EdgeInsets.only(bottom: 16.0),
                    child: getBaseButton(
                        onPressed: state is! HomeProcessing
                            ? _onLoginButtonPressed
                            : null,
                        text: 'Get user')),
                RaisedButton(
                  child: Text('logout'),
                  onPressed: () {
                    BlocProvider.of<AuthenticationBloc>(context)
                        .add(LoggedOut());
                  },
                ),
                Container(
                  child: state is HomeProcessing
                      ? CircularProgressIndicator()
                      : null,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
