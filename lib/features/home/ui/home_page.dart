import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_login/features/home/bloc/bloc.dart';
import 'package:flutter_login/features/home/ui/home_form.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: BlocProvider(
          create: (context) {
            return HomeBloc();
          },
          child: Stack(
            children: <Widget>[
              Center(
                child: HomeForm(),
              ),
            ],
          )),
    );
  }
}
