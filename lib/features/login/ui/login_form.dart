import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_login/features/login/login.dart';
import 'package:flutter_login/features/register/register.dart';
import 'package:flutter_login/widgets/buttons.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter_login/resources/langs/strings.dart';
import '../styles/styles.dart';

class LoginForm extends StatefulWidget {
  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  _showDialog(BuildContext context, title, description, AlertType alertType) {
    // flutter defined function
    // BuildContext context;
    Alert(
      context: context,
      type: alertType,
      title: title,
      desc: description,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }

  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    _onLoginButtonPressed() {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        BlocProvider.of<LoginBloc>(context).add(
          LoginButtonPressed(
            username: _usernameController.text,
            password: _passwordController.text,
          ),
        );
      }
    }

    return EasyLocalizationProvider(
        data: data,
        child: BlocListener<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state is LoginFailure) {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('${state.error}'),
                  backgroundColor: Colors.red,
                ),
              );
            }
          },
          child: BlocBuilder<LoginBloc, LoginState>(
            builder: (context, state) {
              return Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                        decoration: InputDecoration(
                            labelText:
                                AppLocalizations.of(context).tr(S.USERNAME)),
                        controller: _usernameController,
                        onSaved: (value) => _usernameController.text = value,
                        validator: (val) {
                          return val.trim().isEmpty
                              ? AppLocalizations.of(context)
                                  .tr(S.EMPTY_USERNAME)
                              : null;
                        }),
                    TextFormField(
                        decoration: InputDecoration(
                            labelText:
                                AppLocalizations.of(context).tr(S.PASSWORD)),
                        controller: _passwordController,
                        obscureText: true,
                        onSaved: (value) => _passwordController.text = value,
                        validator: (val) {
                          return val.trim().isEmpty
                              ? AppLocalizations.of(context)
                                  .tr(S.EMPTY_PASSWORD)
                              : null;
                        }),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                        child: getBaseButton(
                            onPressed: state is! LoginLoading
                                ? _onLoginButtonPressed
                                : null,
                            text: AppLocalizations.of(context).tr(S.LOGIN))),
                    Padding(
                        padding: EdgeInsets.only(bottom: 0.0),
                        child: getBaseButtonTransparent(
                          onPressed: () {},
                          text:
                              AppLocalizations.of(context).tr(S.RESET_PASSWORD),
                          // textStyle:
                        )),
                    Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                        child: getBaseButtonTransparent(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => RegisterPage()));
                            },
                            text: AppLocalizations.of(context).tr(S.REGISTER),
                            textStyle: Styles.labelGray)),
                    Container(
                      child: state is LoginLoading
                          ? CircularProgressIndicator()
                          : null,
                    ),
                  ],
                ),
              );
            },
          ),
        ));
  }
}
