import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:user_repository/user_repository.dart';

import 'package:flutter_login/authentication/authentication.dart';
import 'package:flutter_login/features/login/login.dart';
import '../styles/styles.dart';
import 'package:flutter_login/features/contact/ui/contact_page.dart';

class LoginPage extends StatelessWidget {
  final UserRepository userRepository;

  LoginPage({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;

    return EasyLocalizationProvider(
        data: data,
        child: Scaffold(
            appBar: AppBar(
              title: Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.contacts,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ContactPage()));
                    },
                  )
                ],
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.language,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Alert(
                        context: context,
                        type: AlertType.none,
                        title: 'ປ່ຽນພາສາ',
                        style: Styles.alertStyle,
                        content: Column(
                          children: <Widget>[
                            DialogButton(
                              child: Text(
                                "ລາວ",
                                style: Styles.DialogButtonLabel,
                              ),
                              onPressed: () {
                                data.changeLocale(Locale("lo", "LA"));
                                Navigator.pop(context);
                              },
                              width: double.infinity,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            DialogButton(
                              child: Text(
                                "English",
                                style: Styles.DialogButtonLabel,
                              ),
                              onPressed: () {
                                data.changeLocale(Locale("en", "US"));
                                Navigator.pop(context);
                              },
                              width: double.infinity,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            DialogButton(
                              child: Text(
                                "中国人",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              onPressed: () => Navigator.pop(context),
                              width: double.infinity,
                            )
                          ],
                        ),
                        buttons: []).show();
                  },
                )
              ],
            ),
            body: BlocProvider(
                create: (context) {
                  return LoginBloc(
                    authenticationBloc:
                        BlocProvider.of<AuthenticationBloc>(context),
                    userRepository: userRepository,
                  );
                },
                child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Container(
                        width: double.infinity,
                        height: double.infinity,
                        child: LoginForm(),
                      ),
                    ),
                  ],
                )),
//          bottomNavigationBar: BottomNavigationBar(
//            currentIndex: 0, // this will be set when a new tab is tapped
//            // backgroundColor: Colors.green,
//            items: [
//              BottomNavigationBarItem(
//                icon: new Icon(Icons.location_on),
//                title: new Text('ສະຖານທີ່ຕັ້ງ'),
//              ),
//              BottomNavigationBarItem(
//                icon: new Icon(Icons.volume_down),
//                title: new Text('ໂປຣໂມຊັນ'),
//              ),
//              BottomNavigationBarItem(
//                  icon: Icon(Icons.more_horiz), title: Text('ເພີ່ມເຕີມ'))
//            ],
//          ),
            bottomNavigationBar: BottomAppBar(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  FlatButton(
                    onPressed: () => {},
                    // color: Colors.orange,
                    // padding: EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.white,
                        ),
                        Text(
                          'ສະຖານທີ່ຕັ້ງ',
                          style: Styles.TextWhite,
                        )
                      ],
                    ),
                  ),
                  FlatButton(
                    onPressed: () => {},
                    // color: Colors.orange,
                    // padding: EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(
                          Icons.volume_down,
                          color: Colors.white,
                        ),
                        Text(
                          'ໂປຣໂມຊັນ',
                          style: Styles.TextWhite,
                        )
                      ],
                    ),
                  ),
                  FlatButton(
                    onPressed: () => {},
                    // color: Colors.orange,
                    // padding: EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.more_horiz,
                          color: Colors.white,
                        ),
                        Text(
                          'ເພີ່ມເຕີມ',
                          style: Styles.TextWhite,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )));
  }
}
