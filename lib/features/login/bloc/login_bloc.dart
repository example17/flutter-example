import 'dart:async';
import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:user_repository/user_repository.dart';

import 'package:flutter_login/authentication/authentication.dart';
import 'package:flutter_login/features/login/bloc/bloc.dart';
import 'package:flutter_login/api/api.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  })  : assert(userRepository != null),
        assert(authenticationBloc != null);

  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();

      try {
        final token = await userRepository.authenticate(
          username: event.username,
          password: event.password,
        );
        authenticationBloc.add(LoggedIn(token: token));
//        yield LoginFailure(error: 'error');
//        if (token == '400') {
//          yield LoginFailure(error: token.toString());
//        } else {
//          authenticationBloc.add(LoggedIn(token: token.toString()));
//          yield LoginInitial();
//        }
      } catch (error) {
        yield LoginFailure(error: error.toString());
      }
    }
  }
}
