import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_login/features/register/register.dart';

class RegisterPage extends StatelessWidget {
  RegisterPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: BlocProvider(
          create: (context) {
            return RegisterBloc();
          },
          child: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: double.infinity,
                child: RegisterForm(),
              ),
            ],
          )),
    );
  }
}
