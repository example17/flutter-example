import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_login/features/register/register.dart';
import 'package:flutter_login/widgets/buttons.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class RegisterForm extends StatefulWidget {
  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _name = TextEditingController();
  final _email = TextEditingController();
  final _username = TextEditingController();
  final _password = TextEditingController();

  @override
  _showDialog(BuildContext context, title, description, AlertType alertType) {
    // flutter defined function
    // BuildContext context;
    Alert(
      context: context,
      type: alertType,
      title: title,
      desc: description,
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }

  @override
  Widget build(BuildContext context) {
    _onRegisterButtonPressed() {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        BlocProvider.of<RegisterBloc>(context).add(
          Register(
            name: _name.text,
            email: _email.text,
            username: _username.text,
            password: _password.text,
          ),
        );
      }
    }

    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state is RegisterFailure) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('${state.error}'),
              backgroundColor: Colors.red,
            ),
          );
        } else if (state is RegisterSuccess) {
          if (state.code.toString() == "400") {
            this._showDialog(context, 'Warning',
                'Warning! \nresponse Code: ' + state.code, AlertType.warning);
          } else {
            Navigator.pop(context);
          }
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return Form(
            key: _formKey,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                    decoration: InputDecoration(labelText: 'FullName'),
                    controller: _name,
                    onSaved: (value) => _name.text = value,
                    validator: (val) {
                      return val.trim().isEmpty ? 'FullName' : null;
                    }),
                TextFormField(
                    decoration: InputDecoration(labelText: 'E-Mail'),
                    controller: _email,
                    onSaved: (value) => _email.text = value,
                    validator: (val) {
                      return val.trim().isEmpty ? 'E-Mail' : null;
                    }),
                TextFormField(
                    decoration: InputDecoration(labelText: 'username'),
                    controller: _username,
                    onSaved: (value) => _username.text = value,
                    validator: (val) {
                      return val.trim().isEmpty ? 'UserName' : null;
                    }),
                TextFormField(
                    decoration: InputDecoration(labelText: 'password'),
                    controller: _password,
                    obscureText: true,
                    onSaved: (value) => _password.text = value,
                    validator: (val) {
                      return val.trim().isEmpty ? 'Password' : null;
                    }),
                SizedBox(
                  height: 20,
                ),
                Padding(
                    padding: EdgeInsets.only(bottom: 16.0),
                    child: getBaseButton(
                        onPressed: state is! RegisterProcessing
                            ? _onRegisterButtonPressed
                            : null,
                        text: AppLocalizations.of(context).tr('Register'))),
                Container(
                  child: state is RegisterProcessing
                      ? CircularProgressIndicator()
                      : null,
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
