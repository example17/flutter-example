import 'dart:async';

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_login/features/register/bloc/bloc.dart';
import 'package:flutter_login/model/register_base.dart';
import 'package:flutter_login/api/api.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc();
  @override
  // TODO: implement initialState
  RegisterState get initialState => RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is Register) {
      yield RegisterInitial();
      yield* _mapRegisterToState(event);
    } else if (event is GetUsers) {
      yield* _mapGetUserToState(event);
    }
  }

  Stream<RegisterState> mapDispatchToState(RegisterEvent event) async* {}

  Stream<RegisterState> _mapRegisterToState(Register event) async* {
    yield RegisterProcessing();

    try {
//        final token = await userRepository.authenticate(
//          username: event.username,
//          password: event.password,
//        );
      // RegisterBase getRegister = event.fullName;
      Map body = {
        "name": event.name,
        "username": event.username,
        "email": event.email,
        "password": event.password
      };

      final int code = await ApiService.userRegister(body);

      yield RegisterSuccess(code: code.toString());
    } catch (error) {
      yield RegisterFailure(error: error.toString());
    }
  }

  Stream<RegisterState> _mapGetUserToState(GetUsers event) async* {
    final String sr = await ApiService.userInfo();
    print('---------------------------------------');
    print(sr);
  }
}
