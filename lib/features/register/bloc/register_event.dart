import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
  @override
  List<Object> get props => [];
}

class Register extends RegisterEvent {
  final String name;
  final String email;
  final String username;
  final String password;

  const Register({
    @required this.name,
    @required this.email,
    @required this.username,
    @required this.password,
  });

  @override
  List<Object> get props => [name, email, username, password];

  @override
  String toString() =>
      'Register { fullname: $name email: $email username: $username, password: $password }';
}

class GetUsers extends RegisterEvent {}
