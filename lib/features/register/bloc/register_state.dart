import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();

  @override
  List<Object> get props => [];
}

class RegisterInitial extends RegisterState {
  @override
  String toString() => 'RegisterStateEmpty';
}

class RegisterProcessing extends RegisterState {
  @override
  String toString() => 'StateProcessing';
}

class RegisterSuccess extends RegisterState {
  final String code;

  const RegisterSuccess({@required this.code});

  @override
  List<Object> get props => [code];

  @override
  String toString() => 'RegisterSuccess { success: $code }';
}

class RegisterFailure extends RegisterState {
  final String error;

  const RegisterFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'RegisterFailure { error: $error }';
}

class GetUserInfo extends RegisterState {}
