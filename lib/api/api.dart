import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_login/config.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String _applicationId = "my_application_id";
// the mobile device unique identity
String _deviceIdentity = "";

/// ----------------------------------------------------------
/// Method which is only run once to fetch the device identity
/// ----------------------------------------------------------
final DeviceInfoPlugin _deviceInfoPlugin = new DeviceInfoPlugin();

Future<String> _getDeviceIdentity() async {
  if (_deviceIdentity == '') {
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo info = await _deviceInfoPlugin.androidInfo;
        _deviceIdentity = "${info.device}-${info.id}";
      } else if (Platform.isIOS) {
        IosDeviceInfo info = await _deviceInfoPlugin.iosInfo;
        _deviceIdentity = "${info.model}-${info.identifierForVendor}";
      }
    } on PlatformException {
      _deviceIdentity = "unknown";
    }
  }

  return _deviceIdentity;
}

Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
Future<String> _getUserToken() async {
  final SharedPreferences prefs = await _prefs;

  return prefs.getString('token') ?? '';
}

class ApiService {
  static Future<int> userRegister(body) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request =
        await client.postUrl(Uri.parse('${Config.API_URL}/auth/signup'));
    request.headers.add("Content-Type", "application/json; charset=utf-8");
    request.add(utf8.encode(jsonEncode(body)));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    if (response.statusCode == 201) {
      return response.statusCode;
    } else {
      return response.statusCode;
    }

//    final response =
//        await http.post('${Config.API_URL}/auth/register', body: body);
//    if (response.statusCode == 201) {
//      return response.statusCode;
//    } else {
//      return response.statusCode;
//    }
  }

  static Future<String> userLogin(body) async {
    // RESPONSE JSON :
    // [{
    //   "id": "1",
    //   "employee_name": "",
    //   "employee_salary": "0",
    //   "employee_age": "0",
    //   "profile_image": ""
    // }]
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request =
        await client.postUrl(Uri.parse('${Config.API_URL}/auth/signin'));
    request.headers.add("Content-Type", "application/json; charset=utf-8");
    // Map map = {"usernameOrEmail": "lamkeo", "password": "12345678"};
    request.add(utf8.encode(json.encode(body)));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    if (response.statusCode == 200) {
      return reply;
    } else {
      return response.statusCode.toString();
    }
//    final response =
//        await http.post('${Config.API_URL}/auth/signin', body: body);
//    if (response.statusCode == 200) {
//      print("-----------------------------------");
//      print(response.body);
//      return response.body;
//    } else {
//      print("-----------------------------------");
//      print(response.body);
//      return response.statusCode.toString();
//    }
  }

  static Future<String> userInfo() async {
//    headers: {
//      'X-DEVICE-ID': await _getDeviceIdentity(),
//      'X-TOKEN': token,
//      'X-APP-ID': _applicationId
//    }
    final token = await _getUserToken();
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request =
        await client.getUrl(Uri.parse('${Config.API_URL}/user/me'));
    request.headers.add("Content-Type", "application/json; charset=utf-8");
    request.headers.add("Authorization", token.toString());
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    if (response.statusCode == 200) {
      return reply;
    } else {
      return response.statusCode.toString();
    }

//    final token = await _getUserToken();
//    final response = await http.get('${Config.API_URL}/users', headers: {
//      'Content-Type': 'application/json; charset=utf-8',
//      'Authorization': token
//    });
//    if (response.statusCode == 200) {
//      return response.body;
//    } else {
//      return response.statusCode.toString();
//    }
  }
}
