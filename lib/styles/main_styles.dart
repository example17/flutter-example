import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

abstract class MainStyles {
  static const paddingAll = EdgeInsets.all(20);

  static var themeData = ThemeData(
      brightness: Brightness.light,
      primaryColor: Colors.green,
      accentColor: Colors.lightGreen,
      bottomAppBarColor: Colors.green);
//  static var text_theme = TextTheme(
//      headline: TextStyle(fontSize: 36, color: Colors.lightGreen),
//      headline1: TextStyle(fontSize: 28, color: Colors.lightGreen),
//      headline2: TextStyle(fontSize: 28, color: Colors.lightGreen),
//      headline3: TextStyle(fontSize: 28, color: Colors.lightGreen),
//      headline4: TextStyle(fontSize: 28, color: Colors.lightGreen),
//      headline5: TextStyle(fontSize: 28, color: Colors.lightGreen),
//      headline6: TextStyle(fontSize: 28, color: Colors.lightGreen),
//      title: TextStyle(fontSize: 24, color: Colors.lightGreen),
//      body1: TextStyle(fontSize: 14, color: Colors.lightGreen),
//      body2: TextStyle(fontSize: 14, color: Colors.lightGreen),
//      bodyText1: TextStyle(fontSize: 14, color: Colors.lightGreen),
//      bodyText2: TextStyle(fontSize: 14, color: Colors.lightGreen),
//      button: TextStyle(fontSize: 14, color: Colors.tealAccent),
//      subhead: TextStyle(fontSize: 14, color: Colors.tealAccent),
//      subtitle: TextStyle(fontSize: 14, color: Colors.tealAccent),
//      subtitle1: TextStyle(fontSize: 14, color: Colors.tealAccent),
//      subtitle2: TextStyle(fontSize: 14, color: Colors.tealAccent),
//      display1: TextStyle(fontSize: 14, color: Colors.green),
//      display2: TextStyle(fontSize: 14, color: Colors.green),
//      display3: TextStyle(fontSize: 14, color: Colors.green),
//      display4: TextStyle(fontSize: 14, color: Colors.green));
}
