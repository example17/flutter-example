import 'package:flutter/material.dart';

SizedBox getBaseButton({String text, VoidCallback onPressed}) {
  return SizedBox(
      height: 48.0,
      width: double.infinity,
      child: RaisedButton(
        child: Text(text),
        onPressed: onPressed,
        textColor: Colors.white,
        color: Colors.green,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
        ),
      ));
}

SizedBox getBaseButtonTransparent(
    {String text, TextStyle textStyle, VoidCallback onPressed}) {
  return SizedBox(
      height: 48.0,
      width: double.infinity,
      child: FlatButton(
        child: Text(
          text,
          style: textStyle,
        ),
        onPressed: onPressed,
        textColor: Colors.black54,
        color: Colors.transparent,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
        ),
      ));
}
